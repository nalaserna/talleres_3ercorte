import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { ListaToDosComponent } from './lista-to-dos/lista-to-dos.component';
import { MatCardModule } from '@angular/material/card';
import { LocacionComponent } from './locacion/locacion.component';
import { PruebaPipesComponent } from './prueba-pipes/prueba-pipes.component';
import { intercalar } from './prueba-pipes/intercalar.pipes';
import { raizcuadrada } from './prueba-pipes/raizcuadrada.pipes';
import { potencia } from './prueba-pipes/potencia.pipes';




@NgModule({
  declarations: [
    AppComponent,
    ListaUsuariosComponent,
    ListaToDosComponent,
    LocacionComponent,
    PruebaPipesComponent,
    intercalar,
    raizcuadrada,
    potencia
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
