import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebaPipesComponent } from './prueba-pipes.component';

describe('PruebaPipesComponent', () => {
  let component: PruebaPipesComponent;
  let fixture: ComponentFixture<PruebaPipesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PruebaPipesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PruebaPipesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
