import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'raizcuadrada'})
export class raizcuadrada implements PipeTransform {
  transform(value: number): number {
      return Math.sqrt(value);
  }
}