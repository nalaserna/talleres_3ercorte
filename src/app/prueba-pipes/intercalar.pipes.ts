import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'intercalar'})
export class intercalar implements PipeTransform {
  transform(value: string): string {
    let newStr: string = "";
    for (var i = 0; i <= value.length -1; i++) {
        if(i % 2 == 0)
      newStr += value.charAt(i).toUpperCase();
        else
        newStr += value.charAt(i).toLowerCase();
    }
    return newStr;
  }
}