import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'potencia'})
export class potencia implements PipeTransform {
  transform(value: number): number {
    return Math.pow(value, 2);
  }
}