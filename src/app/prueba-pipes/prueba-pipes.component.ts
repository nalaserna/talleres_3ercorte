import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prueba-pipes',
  templateUrl: './prueba-pipes.component.html',
  styleUrls: ['./prueba-pipes.component.css']
})
export class PruebaPipesComponent implements OnInit {

  textoPrincipal: string;
  salario: number;
  fecha= Date.now();
  val = 25;

  constructor() {

    this.textoPrincipal= "Hola!";
    this.salario= 1000;
    
   }

  ngOnInit() {
  }

}
