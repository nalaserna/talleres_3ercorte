import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { ListaToDosComponent } from './lista-to-dos/lista-to-dos.component';
import { LocacionComponent } from './locacion/locacion.component';
import { PruebaPipesComponent } from './prueba-pipes/prueba-pipes.component';

const routes: Routes = [
  {path: 'listaUsuarios', component: ListaUsuariosComponent},
  {path: 'listaToDos/:userId/:completed', component: ListaToDosComponent},
  {path: 'locacion/:lat/:lng', component: LocacionComponent},
  {path: 'pipes', component: PruebaPipesComponent},
  {path: '', pathMatch: 'full', redirectTo: 'listaUsuarios'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }