import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { UsuarioService } from 'src/services/usuario.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lista-to-dos',
  templateUrl: './lista-to-dos.component.html',
  styleUrls: ['./lista-to-dos.component.css']
})
export class ListaToDosComponent implements OnInit {

  todos : any[] = [];
  id : string;
  completo : boolean;
  constructor(private http: HttpClient, private servicio: UsuarioService, private router: ActivatedRoute) { 

   }

  ngOnInit() {
    this.id = this.router.snapshot.params['userId'];
    this.completo = this.router.snapshot.params['completed'];

    this.servicio.getToDosByStatus(this.id, this.completo).subscribe(resp =>{
      this.todos= resp;
    })
  }

}
